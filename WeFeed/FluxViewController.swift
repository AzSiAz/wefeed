//
//  ThirdViewController.swift
//  WeFeed
//
//  Created by Stephan Deumier on 28/01/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import UIKit

class FluxViewController: UITableViewController, UITableViewDelegate
{
    var parser = XMLParserTitre()
    var urlstring2 = "http://www.journaldugamer.com/feed/"
    var urlstring = "http://www.journaldugeek.com/feed/"
    var urlstring3 = "http://feeds.feedburner.com/netserv/rss"
    var items:[String] = []
    var itemsSave:[String] = []
    var link:[String] = []
    @IBOutlet weak var titlelabel:UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.refreshControl?.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        loadArticle(urlstring)
        link = parser.link2
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.numberOfLines = 0;
        cell.textLabel?.text = items[indexPath.row]
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue,
        sender: AnyObject!)
    {
        let cell = sender as UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        
        let item = self.link[indexPath!.row]
        let item2 = self.items[indexPath!.row]
        
        let article = segue.destinationViewController as ArticleViewController
        article.item = item
        article.titles = item2
    }
    
    func refresh(sender:AnyObject)
    {
        itemsSave = items
        parser.removeItems()
        refreshArticle()
        if (items != itemsSave)
        {
            println(link)
            self.tableView.reloadData()
        }
        self.refreshControl?.endRefreshing()
    }
    
    func loadArticle(url2parse: NSString)
    {
        parser.setURL(url2parse)
        parser.loadParser()
        items = parser.items
    }
    
    func refreshArticle()
    {
        parser.loadParser()
        items.removeAll(keepCapacity: false)
        //println(items)
        items = parser.items
        //println(items)
    }
}