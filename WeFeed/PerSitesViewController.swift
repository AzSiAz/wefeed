//
//  PerSitesViewController.swift
//  WeFeed
//
//  Created by Stephan Deumier on 01/04/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import UIKit

class PerSitesViewController: UITableViewController {
    
    var url: String?
    var titles: String?
    var parser = XMLParserTitre()
    var items:[String] = []
    var itemsSave:[String] = []
    var link:[String] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //tableView.rowHeight = UITableViewAutomaticDimension
        self.title = titles
        self.refreshControl?.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        loadArticle(url!)
        link = parser.link2
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.numberOfLines = 0;
        cell.textLabel?.text = items[indexPath.row]
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue,
        sender: AnyObject!)
    {
        let cell = sender as UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        
        let item = self.link[indexPath!.row]
        let item2 = self.items[indexPath!.row]
        
        let article = segue.destinationViewController as ArticleViewController
        article.item = item
        article.titles = item2
    }
    
    func refresh(sender:AnyObject)
    {
        itemsSave = items
        parser.removeItems()
        refreshArticle()
        if (items != itemsSave)
        {
            println(link)
            self.tableView.reloadData()
        }
        self.refreshControl?.endRefreshing()
    }
    
    func loadArticle(url2parse: NSString)
    {
        parser.setURL(url2parse)
        parser.loadParser()
        items = parser.items
    }
    
    func refreshArticle()
    {
        parser.loadParser()
        items.removeAll(keepCapacity: false)
        //println(items)
        items = parser.items
        //println(items)
    }
}