//
//  XMLParser.swift
//  WeFeed
//
//  Created by Stephan Deumier on 14/01/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import Foundation

class XMLParserTitre: NSObject ,NSXMLParserDelegate
{
    
    var element:NSString = ""
    var items:[String] = []
    var item = ""
    var link1 = ""
    var link2:[String] = []
    var urlToParse = ""
    
    func loadParser()
    {
        let url = NSURL(string: self.urlToParse)
        var parser = NSXMLParser(contentsOfURL: url)
        parser?.delegate = self
        parser?.shouldProcessNamespaces = true
        parser?.shouldReportNamespacePrefixes = true
        parser?.shouldResolveExternalEntities = true
        parser?.parse()
    }
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!)
    {
        
        self.element = elementName
        if ((elementName as NSString).isEqualToString("item")){
            self.item = ""
            self.link1 = ""
        }
        
    }
    
    func parser(parser: NSXMLParser!, foundCharacters string: String!)
    {
        if ( (self.element.isEqualToString("title")) && (self.element != "") ){
            self.item += string
        }
        
        if ((self.element.isEqualToString("link")) && (self.element != "")){
            self.link1 += string
        }
    }
    
    func parser(parser: NSXMLParser!, didEndElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!)
    {
        
        if ((elementName as NSString).isEqualToString("item")){
            self.items.append(self.item)
            self.link2.append(self.link1)
        }
    }
    
    func setURL(url: NSString)
    {
        self.urlToParse = url
    }
    
    func removeItems()
    {
        self.items = []
        self.link2 = []
    }
}