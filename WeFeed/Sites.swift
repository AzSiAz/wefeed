//
//  Sites.swift
//  WeFeed
//
//  Created by Stephan Deumier on 04/03/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import Foundation
import CoreData

class Sites: NSManagedObject {

    @NSManaged var site: String
    @NSManaged var url: String

}
