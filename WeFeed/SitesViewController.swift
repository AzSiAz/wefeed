//
//  FirstViewController.swift
//  WeFeed
//
//  Created by Stephan Deumier on 14/01/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import UIKit
import CoreData

class SitesViewController: CoreDataTableViewController {
    
    
    @IBOutlet weak var itemAdd: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.editButtonItem();
        itemAdd.enabled = false
        tableView.estimatedRowHeight = 56
        tableView.rowHeight = UITableViewAutomaticDimension
        refreshData("site")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        if (editing) {
            itemAdd.enabled = true
        }
        else {
            itemAdd.enabled = false
        }
        super.setEditing(editing, animated: true)
    }
    
    @IBAction func editButton(sender: AnyObject) {
        setEditing(self.editing, animated: true)
    }
    
    @IBAction func addSites(sender: AnyObject) {
        var alert = UIAlertController(title: "New Site",
            message: "Add a new Site",
            preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
            style: .Default) { (action: UIAlertAction!) -> Void in
                
                let textField1 = alert.textFields![0] as UITextField
                let textField2 = alert.textFields![1] as UITextField
                self.saveNameUrl(textField1.text, url1: textField2.text)
                //self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
            style: .Default) { (action: UIAlertAction!) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (textFields: UITextField!) -> Void in
        }
        alert.addTextFieldWithConfigurationHandler {
            (textFields : UITextField!) -> Void in
        }
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        presentViewController(alert,
            animated: true,
            completion: nil)
    }
    
    func saveNameUrl(site: String, url1: String) {
        Sites.createWithAttributes(["site" : site, "url" : url1])
        AERecord.saveContextAndWait()
    }
    
    func refreshData(attribute: String) {
        let sortSites = [NSSortDescriptor(key: attribute, ascending: false)]
        let request = Sites.createFetchRequest(sortDescriptors: sortSites)
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: AERecord.defaultContext, sectionNameKeyPath: nil, cacheName: nil)
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        if let frc = fetchedResultsController {
            if let object = frc.objectAtIndexPath(indexPath) as? Sites {
                let name = object.site
                let url = object.url
                cell.textLabel?.text = name
                cell.detailTextLabel?.text = url
            }
        }
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue,
        sender: AnyObject!)
    {
        let cell = sender as UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        
        if let frc = fetchedResultsController {
            if let object = frc.objectAtIndexPath(indexPath!) as? Sites {
                let article = segue.destinationViewController as PerSitesViewController
                article.url = object.url
                article.titles = object.site
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // delete object
            if let event = fetchedResultsController?.objectAtIndexPath(indexPath) as? NSManagedObject {
                event.delete()
                AERecord.saveContext()
            }
        }
    }
}