//
//  AutresViewController.swift
//  WeFeed
//
//  Created by Lorenzo Risi on 04/03/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import UIKit

class AutresViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate
{
    var paramettersItems:[String] = ["Crédits","Dons","Notifications","Contact","Réglages",]
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return paramettersItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel?.text = paramettersItems[indexPath.row]
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var viewController: UIViewController?
        
        // Initialisation du view controller
        if (indexPath.row == 0)
        {
             viewController = storyboard.instantiateViewControllerWithIdentifier("credit") as CreditsViewController
        }
        else if (indexPath.row == 1)
        {
            viewController = storyboard.instantiateViewControllerWithIdentifier("dons") as DonsViewController
        }
        else if (indexPath.row == 2)
        {
            viewController = storyboard.instantiateViewControllerWithIdentifier("notifications") as NotificationsViewController
        }
        else if (indexPath.row == 3)
        {
            viewController = storyboard.instantiateViewControllerWithIdentifier("contact") as ContactViewController
        }
        else if (indexPath.row == 4)
        {
            viewController = storyboard.instantiateViewControllerWithIdentifier("reglages") as ReglagesViewController
        }
        
        
        // Affichage du view controller
        if var vc = viewController
        {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue,
        sender: AnyObject!)
    {
        let cell = sender as UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
    }
    

    
    
    
}


