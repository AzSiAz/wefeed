//
//  Data.swift
//  WeFeed
//
//  Created by Stephan Deumier on 04/03/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import Foundation
import CoreData

class Data: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var id_Site: NSNumber
    @NSManaged var title: String
    @NSManaged var url_article: String

}
