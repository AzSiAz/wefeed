//
//  SecondViewController.swift
//  WeFeed
//
//  Created by Stephan Deumier on 14/01/2015.
//  Copyright (c) 2015 Stephan Deumier. All rights reserved.
//

import UIKit

extension String {
    func trim() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}

class ArticleViewController: UIViewController, UIWebViewDelegate
{
    
    var item: String?
    var titles: String?
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = self.titles
        
        if self.item != nil
        {
            webView.scalesPageToFit = true
            println(item!.trim())
            let url = NSURL(string: self.item!.trim())!
            let request = NSURLRequest(URL: url)
            webView.delegate = self
            webView.loadRequest(request)
        }
    }
    func webViewDidStartLoad(webView: UIWebView!) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView: UIWebView!) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func forward(sender: AnyObject) {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func share(sender: AnyObject) {
        let textToShare = "Regarde sa peut t'intéresser"
        
        if let myWebsite = NSURL(string: self.item!.trim())
        {
            let objectsToShare = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func refresh(sender: AnyObject) {
        webView.reload()
    }
    
    @IBAction func save(sender: AnyObject) {
        println("save")
    }

}


